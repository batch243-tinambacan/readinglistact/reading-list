// Return all the documents that have the price equal or greater than 20. (Include ss)

	db.fruits.find({price : {$gte:20}})


// Return all the documents that have stocks greater than 10 and less than 30. (Include ss)
	db.fruits.find({$and: [{stock: {$gt : 10}}, {stock: {$lt : 30}}]})


// find fruits that have a in their name and should be from the supplier_id 1. (include ss)

	db.fruits.find({$and: [{name: {$regex: 'a', $options : '$i'}}, {supplier_id: 1}]})


// update the supplier_id to 1 if the name of the fruit has n. (Include ss)

	db.fruits.updateMany(
		{name: {$regex: 'n', $options : '$i'}},
		{
			$set: {
				supplier_id: 1,
			}
		}
	)

	
// update the price of several books that has stocks less than or equal to 15. The new price should be 200. (Include ss)

		db.fruits.updateMany(
			{stock: {$lte:15}},
			{
				$set: {
					price: 200,
				}
			}
		)

