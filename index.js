// Import the http module using the required directive. Create a variable port and assign it with the value 8000.

// Create a server using the createServer() that will listen in to the port provided above.

// Console log in the terminal a mesage when the server is successfully running.

// Create a condition that when the login route, the register route, the homepage and the user profile is accessed, it will print a message specific to the page accessed.

// Access the routes to test if it's working as intended. SAve a ss for each test.

// Create a condition for any other routes that will return an error message.

const http = require("http")
const port = 8000;

const server = http
  .createServer((req, res) => {
    if (req.url == "/login") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Welcome to the login page!");
    }

    if (req.url == "/register") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("Sign up and register with us!");
    }

    if (req.url == "/homepage") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("You are in the homepage.");
    }

    if (req.url == "/userProfile") {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("You are in the user profile page.");
    }

    else {
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end("I'm sorry, the page you are looking for cannot be found.");
    }

})
  .listen(port);

console.log(`Server successfully running at localhost: ${port}`);